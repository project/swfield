
SWField provides an "Flash" widget type to CCK. This modules leverages the
functionality of FileField and behaves nearly identically. SWField widgets
will give you a nice thumbnail preview of the movie when uploaded. Display
options (formatters) within CCK are provided by SWF Tools, a module which
this one dependends on. While a few formatters could have been implemented
natively in this module, it has been chosen to depend on SWF Tools because
this leaves open a handful of options to improve the widget in the future,
such as the support for a HTML alternative, support for movie players, to
mention a few.

SWField was written by Vincenzo Russo (Vincenzo @ Drupal, enzoru @ IRC).
It currently is still mantained by Vincenzo Russo.

Dependencies
------------
 * FileField
 * Content
 * SWF Tools

SWField also provides additional features when used with the following:

 * Insert (Allows users to embed files directly into the body text editor)

Install
-------

1) Copy the swfield folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

3) Create a new swf field in through CCK's interface. Visit Administer ->
   Content management -> Content types (admin/content/types), then click
   Manage fields on the type you want to add an image upload field. Select
   "File" as the field type and "Image" as the widget type to create a new
   field.
