<?php

/**
 * @file
 * SWField widget hooks and callbacks.
 */

/**
 * Implementation of CCK's hook_widget_settings($op = 'form').
 */
function swfield_widget_settings_form($widget) {
  $form = module_invoke('filefield', 'widget_settings', 'form', $widget);

  if ($form['file_extensions']['#default_value'] == 'txt') {
    $form['file_extensions']['#default_value'] = 'swf';
  }

  // Default image settings.
  $form['flash_size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dimensions'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 2
  );

  $form['flash_size']['flash_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => !empty($widget['flash_width']) ? $widget['flash_width'] : 0,
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The width for SWF files.'),
    '#weight' => 2.1,
    '#required' => TRUE,
  );
  $form['flash_size']['flash_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => !empty($widget['flash_height']) ? $widget['flash_height'] : 0,
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The height for SWF files.'),
    '#weight' => 2.2,
    '#required' => TRUE,
  );

  $form['flash_size']['flash_allow_size_ovveride'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow dimensions override'),
    '#default_value' =>  $widget['flash_allow_size_ovveride'],
    '#description' => t('If enabled, users will be given the possibility to ovveride the dimensions of the file being uploaded.'),
    '#weight' => 2.3
  );

  // Default image settings.
  $form['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Flash Movie'),
    '#element_validate' => array('_swfield_widget_settings_default_validate'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 3
  );

  // Present a thumbnail of the current default movie.
  $form['default']['use_default_file'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use default file'),
    '#default_value' =>  $widget['use_default_file'],
    '#description' => t('When no file is uploaded, show a default Flash movie on display.'),
  );

  if (!empty($widget['default_file'])) {
    list($t_width, $t_height) = explode('x', variable_get('swfield_thumb_size', '150x150'));
    $form['default']['default_file_thumbnail'] = array(
      '#type' => 'markup',
      '#value' => theme('swfield_swf', $widget['default_file'], $t_width, $t_height),
    );
  }
  $form['default']['default_file_upload'] = array(
    '#type'  => 'file',
    '#title' => t('Upload movie'),
    '#description' => t('Choose a Flash movie that will be used as default.'),
  );

  // We set this value on 'validate' so we can get CCK to add it
  // as a standard field setting.
  $form['default_file'] = array(
    '#type' => 'value',
    '#value' => $widget['default_file'],
  );

  return $form;
}

/**
 * Simple utility function to check if a file is an SWF.
 */
function _swfield_file_is_swf($file) {
  $errors = array();
  $file = (object)$file;
  if (!in_array($file->filemime, array('application/x-shockwave-flash'))) {
    $errors[] = t('Only Shockware Flash (SWF) files are allowed.');
  }
  return $errors;
}

/**
 * Element specific validation for swfield default value.
 *
 * Validated in a separate function from swfield_field() to get access
 * to the $form_state variable.
 */
function _swfield_widget_settings_default_validate($element, &$form_state) {
  // Skip this validation if there isn't a default file uploaded at all.
  if (!is_uploaded_file($_FILES['files']['tmp_name']['default_file_upload'])) {
    return;
  }

  // Verify the destination exists.
  $destination = file_directory_path() .'/swfield_default_flashmovies';
  if (!field_file_check_directory($destination, FILE_CREATE_DIRECTORY)) {
    form_set_error('default_file', t('The default movie could not be uploaded. The destination %destination does not exist or is not writable by the server.', array('%destination' => dirname($destination))));
    return;
  }

  $validators = array(
    '_swfield_file_is_swf' => array(),
  );

  // We save the upload here because we can't know the correct path until the file is saved.
  if (!$file = file_save_upload('default_file_upload', $validators, $destination)) {
    // No upload to save we hope... or file_save_upload() reported an error on its own.
    return;
  }

  // Remove old file (if any) & clean up database.
  $old_default = $form_state['values']['default_file'];
  if (!empty($old_default['fid'])) {
    if (file_delete(file_create_path($old_default['filepath']))) {
      db_query('DELETE FROM {files} WHERE fid = %d', $old_default['fid']);
    }
  }

  // Make the file permanent and store it in the form.
  file_set_status($file, FILE_STATUS_PERMANENT);
  $file->timestamp = time();
  $form_state['values']['default_file'] = (array)$file;
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'validate').
 */
function swfield_widget_settings_validate($widget) {
  // Check that only SWF files are specified in the callback.
  $extensions = array_filter(explode(' ', $widget['file_extensions']));
  $swf_extensions = array('swf');
  if (count(array_diff($extensions, $swf_extensions))) {
    form_set_error('file_extensions', t('Only Flash Files (swf) are supported through the Flash File widget. If needing to upload other types of files, change the widget to use a standard file upload.'));
  }

  // Check that set dimensions are valid.
  foreach (array('width', 'height') as $dimension) {
    if (!empty($widget['flash_' . $dimension]) && !preg_match('/^[0-9]+$/', $widget['flash_' . $dimension])) {
      form_set_error('flash_' . $dimension, t('Please use numbers to specify the ' . $dimension));
    }
  }
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'save').
 */
function swfield_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);
  return array_merge($filefield_settings, array('flash_width', 'flash_height', 'flash_allow_size_ovveride', 'default_file', 'default_file_upload', 'use_default_file'));
}

/**
 * Element #value_callback function.
 */
function swfield_widget_value($element, $edit = FALSE) {
  $item = filefield_widget_value($element, $edit);
  $field = content_fields($element['#field_name'], $element['#type_name']);
  $widget = $field['widget'];

  if ($edit) {
    $item['flash_width'] = isset($edit['flash_width']) ? $edit['flash_width'] : '';
    $item['flash_height'] = isset($edit['flash_height']) ? $edit['flash_height'] : '';
  }
  else {
    $item['flash_width'] = '';
    $item['flash_height'] = '';
  }

  return $item;
}

/**
 * Element #process callback function.
 */
function swfield_widget_process($element, $edit, &$form_state, $form) {
  $file = $element['#value'];
  $field = content_fields($element['#field_name'], $element['#type_name']);

  $element['#theme'] = 'swfield_widget_item';

  if (isset($element['preview']) && $element['#value']['fid'] != 0) {
    $element['preview']['#value'] = theme('swfield_widget_preview', $element['#value']);
  }

  // Check whether or not we're allowing dimensions overriding 
  $not_override = !$field['widget']['flash_allow_size_ovveride'];
  if ($not_override) {
    $field['widget']['flash_width'] = empty($field['widget']['flash_width']) ? '' : $field['widget']['flash_width'];
    $field['widget']['flash_height'] = empty($field['widget']['flash_height']) ? '' : $field['widget']['flash_height'];
  }
  $element['data']['flash_width'] = array(
    '#title' => t('Width'),
    '#type' => $not_override ? 'value' : 'textfield',
    '#default_value' => $not_override ? $field['widget']['flash_width'] : (is_numeric($file['data']['flash_width']) ? $file['data']['flash_width'] : $field['widget']['flash_width']),
    '#description' => t('This value will set the width for the SWF file.'),
    '#maxlength' => 40,
    '#attributes' => array('class' => 'swfield-text'),
  );
  $element['data']['flash_height'] = array(
    '#title' => t('Height'),
    '#type' => $not_override ? 'value' : 'textfield',
    '#default_value' => $not_override ? $field['widget']['flash_height'] : (is_numeric($file['data']['flash_height']) ? $file['data']['flash_height'] : $field['widget']['flash_height']),
    '#description' => t('This value will set the height for the SWF file.'),
    '#maxlength' => 40,
    '#attributes' => array('class' => 'swfield-text'),
  );
  // #value must be hard-coded if #type = 'value'.
  if ($not_override) {
    $element['data']['flash_width']['#value'] = $field['widget']['flash_width'];
    $element['data']['flash_height']['#value'] = $field['widget']['flash_height'];
  }
  else {
    if (!is_numeric($file['data']['flash_height'])) {
      $element['data']['flash_height']['#value'] = $field['widget']['flash_height'];
    }
    if (!is_numeric($file['data']['flash_width'])) {
      $element['data']['flash_width']['#value'] = $field['widget']['flash_width'];
    }
  }

  return $element;
}

/**
 * FormAPI theme function. Theme the output of an image field.
 */
function theme_swfield_widget($element) {
  drupal_add_css(drupal_get_path('module', 'swfield') .'/swfield.css');
  $element['#id'] .= '-upload'; // Link the label to the upload field.
  return theme('form_element', $element, $element['#children']);
}
