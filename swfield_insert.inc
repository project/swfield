<?php

/**
 *  Implementation of hook_insert_styles
 *
 *  Provide a new insert style (via Insert module) for the SWField CCK field.
 *  This style is supposed to be used for embedding flash files uploaded via the aforementioned field.
 *
 *  @see insert_styles()
 */
function swfield_insert_styles() {
  $styles = array();
  $styles['embed_flash'] = array('label' => 'Embed Flash Files', 'weight' => -9);
  return $styles;
}

/**
 *  Implementation of hook_insert_content
 *
 *  Provide the content to be inserted when the Embed Flash Files insert style will be used
 */
function swfield_insert_content($item, $style, $widget) {
  if ($widget['flash_allow_size_ovveride']) {
    return theme('swfield_swf', $item, '__width__', '__height__');
  }
  else {
    return theme('swfield_swf', $item, $widget['flash_width'], $widget['flash_height']);
  }
}

/**
 * Implementation of hook_insert_widgets().
 */
function swfield_insert_widgets() {
  return array(
    'swfield_widget' => array(
      'wrapper' => '.filefield-element',
      'fields' => array(
        'width' => 'input[name$="[flash_width]"], textarea[name$="[flash_width]"]',
        'height' => 'input[name$="[flash_height]"], textarea[name$="[flash_height]"]',
        'description' => 'input[name$="[description]"], textarea[name$="[description]"]',
      ),
    ),
  );
}
